docker-compose -f quickstart.yml exec hydra \
    hydra clients create \
    --endpoint http://127.0.0.1:4445/ \
    --id my-client \
    --secret secret \
    -g client_credentials

docker-compose -f quickstart.yml exec hydra \
    hydra token client \
    --endpoint http://127.0.0.1:4444/ \
    --client-id my-client \
    --client-secret secret

docker-compose -f quickstart.yml exec hydra \
    hydra token introspect \
    --endpoint http://127.0.0.1:4445/ \
    --client-id my-client \
    --client-secret secret \
G82eELjHH9fcE2F-A4ea_grbH9dhfK6YD5LrFjn-vlc.uPiK49e2-XmnQFwH_enhHnMjZ2b59Os2QxxmgCQGsj8

docker-compose -f quickstart.yml exec hydra \
    hydra clients create \
    --endpoint http://127.0.0.1:4445 \
    --id auth-code-client \
    --secret secret \
    --token-endpoint-auth-method none \
    --grant-types authorization_code,refresh_token \
    --response-types code,id_token \
    --scope openid,offline \
    --callbacks http://127.0.0.1:5555/

docker-compose -f quickstart.yml exec hydra \
    hydra token user \
    --client-id auth-code-client \
    --client-secret secret \
    --endpoint http://127.0.0.1:4444/ \
    --port 5555 \
    --scope openid,offline

docker-compose -f quickstart.yml exec hydra \
    hydra token introspect \
    --endpoint http://127.0.0.1:4445/ \
    --client-id auth-code-client \
    --client-secret secret \
ZU7PuulXNE_oVRaAtr9StKPpHLziJ5hnb3RpweRwfxU.9rX0uUCRohAVgjzyP41tzUI8lV_KhvkA6frzpys9Yas

docker-compose -f quickstart.yml exec hydra \
    hydra token introspect \
    --endpoint http://127.0.0.1:4445/ \
    --client-id my-client \
ZU7PuulXNE_oVRaAtr9StKPpHLziJ5hnb3RpweRwfxU.9rX0uUCRohAVgjzyP41tzUI8lV_KhvkA6frzpys9Yas
